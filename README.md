# Concise Problem Details: Body Error Position

This is the working area for the individual Internet-Draft, "Concise Problem Details: Body Error Position".

* [Datatracker Page](https://datatracker.ietf.org/doc/draft-amsuess-core-pd-body-error-position)
* [Individual Draft](https://datatracker.ietf.org/doc/html/draft-amsuess-core-pd-body-error-position)


## Contributing

See the
[guidelines for contributions](CONTRIBUTING.md).

Contributions can be made by creating pull requests.
The GitLab interface supports creating pull requests using the Edit (✏) button.


## Command Line Usage

Formatted text and HTML versions of the draft can be built using `make`.

```sh
$ make
```

Command line usage requires that you have the necessary software installed.  See
[the instructions](https://github.com/martinthomson/i-d-template/blob/main/doc/SETUP.md).

