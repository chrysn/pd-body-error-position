---
title: "Concise Problem Details: Body Error Position"
docname: draft-amsuess-core-pd-body-error-position-latest
ipr: trust200902
stand_alone: true
cat: info
wg: CoRE
author:
- ins: C. Amsüss
  name: Christian Amsüss
  country: Austria
  email: christian@amsuess.com
normative:
  RFC9290:
  RFC7959:
informative:
  STD94:
  RFC7252:


--- abstract

This defines a single standard problem detail for use with the Concise Problem Details format:
Request Body Error Position.
Using this detail,
the server can point at the position inside the client's request body
that induced the error.

--- middle

# Introduction {#introduction}

Concise Problem Details for CoAP APIs {{RFC9290}} describes
how a server can provide details about an error processing a client request,
and how to extend these error messages.
This document uses that extension mechanism and adds the Request Body Error Position detail.

## Terminology

The description of the problem detail uses the term "body"
as defined in {{RFC7959}}.

## Document lifecycle

Registering a standard problem detail merely requires a specification,
not an RFC (let alone of a particular track),
and has been performed based on version -00 of this document.

Publication as an RFC has not been pursued in -00, nor is it at the time of writing.
It will expire as an Inetnet Draft,
but nonetheless be usable as the permanent reference for the IANA registration.

Should a need for further development or a more official publication arise,
the document may be picked up again at a later time.
For example, that might be done in the style of {{?I-D.bormann-cbor-notable-tags}}.

# Request Body Error Position

The Request Body Error Position problem detail indicates that
the error described by the Concise Problem Details response resulted from processing the request body.
The numeric value indicates a byte position inside that body that corresponds to the error.
The precise error position for invalid data may vary by implementation --
for example,
if a numeric value inside a CBOR ({{STD94}}) item exceeds the expected range,
it may indicate the number's initial byte
(typically if the implementation doesn't even implement the indicated argument size)
or the argument
(if it implements it).

When the request's content format indicated a non-identity content coding,
the offset points into the uncompressed body.
Consequently, this error detail is not suitable for pointing out errors that occur during uncompressing.

The main envisioned use of this option is
for the client to highlight
or back-annotate (eg. to counteract minification, or to display it on some diagnostic notation)
the erroneous item in the request body for a human author.

# Usage example

The figures in this section illustrate a CoAP {{RFC7252}} message exchange using CBOR {{STD94}} bodies,
and a hypothetical CoAP tool's output that utilizes this error detail.

~~~
Req: FETCH coap://example.com/alpha/archive
Content-Format: 60 (application/cbor)
Payload:
  A2071A000123A0182C192118
Payload (diagnostic notation):
  {7: 74656, 44: 8472}

Res: 4.00 Bad Request
Content-Format: 257 (application/concise-problem-details+cbor)
Payload:
  A22071556E6B6E6F776E207175657279206B6579381808
Payload (diagnostic notation):
  {
    / title /                        -1: "Unknown query key",
    / request-body-error-position / -25: 8
  }
~~~
{: title='Messages exchanged between client and server'}

~~~
$ coap post coap://example.com/alpha/archive cbor '{7: 74656, 44: 8472}'
Error: Bad Request: Unknown query key
{7: 74656, 44: 8472}
           ^^ The server indicated that the error occurred here.
~~~
{: title='Output of a hypothetical CoAP client that utilizes the Request Body Error Position detail'}

# Security Considerations

Producing a Request Body Error Position detail
gives the client some information about the internal workings of the server.
If application designers intend to minimize the amount of information obtainable about the server,
they need to weigh that goal against usability,
and may prefer not to expose this (or any other) detail.

The Request Body Error Position detail can be used by malicious clients
to explore the borders of acceptable content.
This can be mitigated by limiting this (or other) details to suitably authorized users,
or, where possible, only parsing data from trusted sources in the first place.

# IANA considerations

A new entry has been assigned in the "Standard Problem Detail Keys" subregistry
of the "Constrained RESTful Environments (CoRE) Parameters" registry.

Key value:
: The value -25 is suggested

Name:
: request-body-error-position

CDDL type:
: uint

Brief description:
: Byte index inside the request body at which the error became apparent

Reference:
: This document

Change controller:
: IETF CoRE working group

# Acknowledgements

Michael Richardson provided good input for the Securitiy Considerations.

--- back

# Change log

Since -00:

* Update IANA section to reflect registration having been performed.
* Update document lifecycle accordingly, mention possibility of a notable-problem-details document.
* Added security considerations.
